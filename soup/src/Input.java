import java.io.*;
import java.util.ArrayList;

public class Input {
	String path;
	ArrayList<String> fullList;
	int rows;
	int columns;
	char[][] wordGrid;
	String[] searchWords;

	//Constructor will build everything for us
	public Input(String path) {
		this.path = path;
		fullList = new ArrayList<>();
		listCreator();
		dimensionGetter();
		wordGrid = new char[rows][columns];
		setGrid();
		searchWords = new String[fullList.size() - (rows + 1)];
		getSearchWords();
	}

	//Takes the txt file and creates an ArrayList
	private void listCreator() {
		try {
			File file = new File(path);

			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			String line = bufferedReader.readLine();
			while (line != null) {
				this.fullList.add(line);
				line = bufferedReader.readLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (IOException f) {
			System.out.println(f.getStackTrace());
		}
	}

	//Gives us the rows and columns
	private void dimensionGetter() {
		String[] temp = fullList.get(0).split("x");
		rows = Integer.parseInt(temp[0]);
		columns = Integer.parseInt(temp[1]);
	}

	//Creates the word grid we will be searching
	private void setGrid() {
		for (int i = 0; i < rows; i++) {
			String string = fullList.get(i + 1).replaceAll("\\s", "");
			for (int j = 0; j < columns; j++) {
				wordGrid[i][j] = string.charAt(j);
			}
		}
	}

	//Gets all words we will be looking for
	private void getSearchWords() {
		for (int i = 0; i < (fullList.size() - (rows + 1)); i++) {
			searchWords[i] = fullList.get(rows + 1 + i);
		}

	}
}
