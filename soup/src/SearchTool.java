public class SearchTool {
	Input input;
	String[] words;
	char[][] grid;
	int rows;
	int columns;
	String start;
	String end;
	boolean foundWord;
	boolean breakPlease;

	//Letting Constructor do it's work
	SearchTool(String path) {
		input = new Input(path);
		words = input.searchWords;
		grid = input.wordGrid;
		this.rows = input.rows;
		this.columns = input.columns;
		wordGetter();
	}

	//Starts the sequence
	public void wordGetter() {
		for (String word : words) {
			findLetters(word);
			foundWord = false;
		}
	}

	//Finds first letter, and if found will pass on the grid coordinates to follow on methods
	public void findLetters(String word) {
		int x = 0;
		int wordLength = word.length();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (grid[i][j] == word.charAt(x)) {
					start = i + ":" + j;
					//added check for foundWord in every if statement to break out of loop faster
					if (j <= (columns - wordLength)) {
						checkRight(i, j, word);
						if (foundWord) {
							break;
						}
					}
					if (i <= (rows - wordLength)) {
						checkDown(i, j, word);
						if (foundWord) {
							break;
						}
					}
					if ((i <= (rows - wordLength)) && (j <= (columns - wordLength))) {
						checkDownAndRight(i, j, word);
						if (foundWord) {
							break;
						}
					}
					if (j > (columns - wordLength)) {
						checkLeft(i, j, word);
						if (foundWord) {
							break;
						}
					}
					if (i > (rows - wordLength)) {
						checkUp(i, j, word);
						if (foundWord) {
							break;
						}
					}
					if ((i > (rows - wordLength)) && ((j > (columns - wordLength)))) {
						checkUpAndLeft(i, j, word);
						if (foundWord) {
							break;
						}
					}
					if ((i > (rows - wordLength)) && ((j <= (columns - wordLength)))) {
						checkUpAndRight(i, j, word);
						if (foundWord) {
							break;
						}
					}
					if ((i <= (rows - wordLength)) && (j > (columns - wordLength))) {
						checkDownAndLeft(i, j, word);
						if (foundWord) {
							break;
						}
					}
				}
			}
			//added a break statement here to completely come out of the loop if word is found.found
			if (foundWord) {
				break;
			}
		}
	}

	//These will check the direction as indicated by method name
	private void checkRight(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			j++;
			charCheck(i, j, word, k);
			//if char doesn't match, this will break it out of the for loop
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	private void checkDown(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			i++;
			charCheck(i, j, word, k);
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	private void checkDownAndRight(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			j++;
			i++;
			charCheck(i, j, word, k);
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	private void checkLeft(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			j--;
			charCheck(i, j, word, k);
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	private void checkUp(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			i--;
			charCheck(i, j, word, k);
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	private void checkUpAndLeft(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			j--;
			i--;
			charCheck(i, j, word, k);
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	private void checkUpAndRight(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			j++;
			i--;
			charCheck(i, j, word, k);
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	private void checkDownAndLeft(int i, int j, String word) {
		for (int k = 1; k < word.length(); k++) {
			j--;
			i++;
			charCheck(i, j, word, k);
			if (breakPlease) {
				breakPlease = false;
				break;
			}
		}
	}

	//Compares the chars, uses parameters passed in to reduce duplication of code
	private void charCheck(int i, int j, String word, int k) {
		if (word.charAt(k) == grid[i][j]) {
			if (k == (word.length() - 1)) {
				foundWord = true;
				end = i + ":" + j;
				System.out.println(word + " " + start + " " + end);
			}
		} else {
			//if char doesn't match it will bring it out of the for loop of the findLetters method.
			breakPlease = true;
		}

	}
}
